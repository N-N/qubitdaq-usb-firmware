#ifndef _INCLUDED_CYFXSLFIFOASYNC_H_
#define _INCLUDED_CYFXSLFIFOASYNC_H_

//#define _USB_DEBUG_

#include "cyu3externcstart.h"
#include "cyu3types.h"
#include "cyu3usbconst.h"

#define SLFIFO_P2U_DMA_BUF_SIZE_MULT	(16)	/* Number of USB packets per buffer for device to host transfers */
#define SLFIFO_U2P_DMA_BUF_SIZE_MULT	(1) 	/* Number of USB packets per buffer for host to device transfers */
#define SLFIFO_DMA_BUF_COUNT      		(2)     /* Slave FIFO channel buffer count */

#define CY_FX_SLFIFO_THREAD_STACK       (0x0800)/* Slave FIFO application thread stack size */
#define CY_FX_SLFIFO_THREAD_PRIORITY    (8)     /* Slave FIFO application thread priority */

/* Endpoint and socket definitions for the Slave FIFO application */

/* To change the Producer and Consumer EP enter the appropriate EP numbers for the #defines.
 * In the case of IN endpoints enter EP number along with the direction bit.
 * For eg. EP 6 IN endpoint is 0x86
 *     and EP 6 OUT endpoint is 0x06.
 * To change sockets mention the appropriate socket number in the #defines. */

/* Note: For USB 2.0 the endpoints and corresponding sockets are one-to-one mapped
         i.e. EP 1 is mapped to UIB socket 1 and EP 2 to socket 2 so on */

#define CY_FX_EP_PRODUCER               0x01    /* EP 1 OUT */
#define CY_FX_EP_CONSUMER               0x81    /* EP 1 IN */
#define CY_FX_EP_DEBUG                  0x82    /* EP 2 IN */

#define CY_FX_PRODUCER_USB_SOCKET    CY_U3P_UIB_SOCKET_PROD_1    /* USB Socket 1 is producer */
#define CY_FX_CONSUMER_USB_SOCKET    CY_U3P_UIB_SOCKET_CONS_1    /* USB Socket 1 is consumer */
#define CY_FX_EP_DEBUG_SOCKET        CY_U3P_UIB_SOCKET_CONS_2    /* Socket 2 is consumer */


/* Used with FX3 Silicon. */
#define CY_FX_PRODUCER_PPORT_SOCKET    CY_U3P_PIB_SOCKET_0    /* P-port Socket 0 is producer */
#define CY_FX_CONSUMER_PPORT_SOCKET    CY_U3P_PIB_SOCKET_3    /* P-port Socket 3 is consumer */

/*Buffer size for the Endpoint 0, must be a multiple of 16*/
#define EP0_BUFFER_SIZE (4096)

/* USB vendor requests supported by the application. */

/* USB vendor request to read the 8 byte firmware ID. This will return content
 * of glFirmwareID array. */
#define RQT_ID_CHECK               (0xB0)

/* USB vendor request to write to I2C. */
#define RQT_I2C_WRITE              (0xBA)

/* USB vendor request to read from I2C. */
#define RQT_I2C_READ               (0xBB)

/* USB vendor request to reset the device */
#define RQT_RESET 					(0xE0)

/* Initiate FPGA configuration. */
#define RQT_FPGA_CONFIG_MODE_ON		(0xBC)

/* Deinitiate FPGA configuration mode. */
#define RQT_FPGA_CONFIG_MODE_OFF	(0xBD)

/* Check the status of nSTATUS and CONF_DONE FPGA pins. */
#define RQT_FPGA_CONFIG_CHECK 		(0xBE)

/* Reset I2C DMA channels */
#define RQT_I2C_RESET				(0xBF)

/* Mapping of the FPGA configuration pins to GPIO pins. */
#define GPIO_CONF_DONE		(50)
#define GPIO_nCONFIG		(51)
#define GPIO_nSTAT			(52)
#define GPIO_DCLK			(26)

/* Extern definitions for the USB Descriptors */
extern const uint8_t CyFxUSB20DeviceDscr[];
extern const uint8_t CyFxUSB30DeviceDscr[];
extern const uint8_t CyFxUSBDeviceQualDscr[];
extern const uint8_t CyFxUSBFSConfigDscr[];
extern const uint8_t CyFxUSBHSConfigDscr[];
extern const uint8_t CyFxUSBBOSDscr[];
extern const uint8_t CyFxUSBSSConfigDscr[];
extern const uint8_t CyFxUSBStringLangIDDscr[];
extern const uint8_t CyFxUSBManufactureDscr[];
extern const uint8_t CyFxUSBProductDscr[];
extern const uint8_t CyFxUSBSerialDscr[];

/* This function creates the serial number string descriptor from the unique 64-bit device ID. */
extern uint8_t* MkSerialStringDescriptor(void);

#include "cyu3externcend.h"

#endif /* _INCLUDED_CYFXSLFIFOASYNC_H_ */

/*[]*/
