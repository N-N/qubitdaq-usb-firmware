#ifndef _INCLUDED_GPIF_SLAVE_H_
#define _INCLUDED_GPIF_SLAVE_H_

/*Watermarks values for GPIF partial flags */
#define THREAD_0_WATERMARK (10) /* Data transfer from GPIF*/
#define THREAD_3_WATERMARK (6)	/* Data transfer to GPIF */

extern void GpifSlaveInit (void);

#endif /* _INCLUDED_GPIF_SLAVE_H_ */
