/*
 * GPIF synchronous slave FIFO mode initialization.
 */

#include "cyu3system.h"
#include "cyu3error.h"
#include "cyu3gpif.h"
#include "cyfxslfifosync.h"

#include "gpif_slave_config.h"
#include "gpif_slave.h"

void
GpifSlaveInit (void)
{
    CyU3PReturnStatus_t apiRetStatus = CY_U3P_SUCCESS;

    /* Load the GPIF configuration for Slave FIFO sync mode. */
    apiRetStatus = CyU3PGpifLoad (&CyFxGpifConfig);
    if (apiRetStatus != CY_U3P_SUCCESS)
    {
        CyU3PDebugPrint (4, "CyU3PGpifLoad failed, Error Code = %d\n\r",apiRetStatus);
    }

    apiRetStatus = CyU3PGpifSocketConfigure (0,CY_U3P_PIB_SOCKET_0,THREAD_0_WATERMARK,CyFalse,1);
    if (apiRetStatus != CY_U3P_SUCCESS)
    {
       CyU3PDebugPrint (4, "CyU3PGpifSocketConfigure failed for socket 0, Error Code = %d\n\r",apiRetStatus);
    }
    apiRetStatus = CyU3PGpifSocketConfigure (3,CY_U3P_PIB_SOCKET_3,THREAD_3_WATERMARK,CyFalse,1);
    if (apiRetStatus != CY_U3P_SUCCESS)
    {
       CyU3PDebugPrint (4, "CyU3PGpifSocketConfigure failed for socket 3, Error Code = %d\n\r",apiRetStatus);
    }
    /* Start the state machine. */
    apiRetStatus = CyU3PGpifSMStart (RESET, ALPHA_RESET);
    if (apiRetStatus != CY_U3P_SUCCESS)
    {
        CyU3PDebugPrint (4, "CyU3PGpifSMStart failed, Error Code = %d\n\r",apiRetStatus);
    }
}



