
#ifndef _INCLUDED_USB_I2C_H_
#define _INCLUDED_USB_I2C_H_

#include "cyu3types.h"
#include "cyu3usbconst.h"
#include "cyu3externcstart.h"

/*7-bit I2C addresses*/
#define FPGA_I2C_DEV_ADDR (0x7f)
#define USER_LED_I2C_DEV_ADDR (0x7F)

/* I2C Data rate */
#define CY_FX_USBI2C_I2C_BITRATE        (400000)

/* Give a timeout value of 5s for any programming. */
#define CY_FX_USB_I2C_TIMEOUT                (5000)

extern void I2C_DMA_Reset(void);
extern CyU3PReturnStatus_t CyFxUsbI2cInit (void);
extern CyU3PReturnStatus_t CyFxUsbI2cTransfer (uint32_t, uint8_t, uint16_t  , uint8_t  *, uint16_t, CyBool_t  );

#include "cyu3externcend.h"

#endif /* _INCLUDED_USB_I2C_H_ */

/*[]*/
