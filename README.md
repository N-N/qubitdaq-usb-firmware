# QubitDAQ USB firmware

This is the firmware for the Infeneon FX3 USB chip. It's created as a part of the QubitDAQ project and provides USB interface with an FPGA within TSW14J56 development board. To access control registers inside the FPGA, the I2C interface is used. For transferring of a large data, the GPIF II synchronous slave FIFO interface is used. Also, the FPGA configuration capability via USB using 32-bit FPP mode is implemented. The firmware was created using FX3 SDK. 

## USB interface

The USB interface has 1 configuration and supports standard control transfers and also has 2 bulk endpoints: IN and OUT. Control transfers are used, in particular, for communicating with the FPGA via I2C. For transferring of a large data via GPIF II interface the bulk endpoints are used.

## Device identification

Each device has a unique serial number USB string descriptor created from 64-bit chip ID.
