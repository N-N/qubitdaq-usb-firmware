/* Main file. */

#include "cyu3system.h"
#include "cyu3os.h"
#include "cyu3dma.h"
#include "cyu3error.h"
#include "cyu3usb.h"
#include "cyu3uart.h"
#include "cyfxslfifosync.h"
#include "cyu3gpif.h"
#include "cyu3pib.h"
#include "cyu3utils.h"
#include "pib_regs.h"
#include <cyu3gpio.h>

#include "usb_i2c.h"
#include "gpif_master.h"
#include "gpif_slave.h"

/* Firmware ID. */
const uint8_t glFirmwareID[32]  __attribute__ ((aligned (32))) = { 'Q','u','b','i','t',' ','D','A','Q','\0' };

/*Buffer for the Endpoint 0*/
uint8_t glEp0Buffer[EP0_BUFFER_SIZE] __attribute__ ((aligned (32)));

CyU3PThread slFifoAppThread;	        /* Slave FIFO application thread structure */
CyU3PDmaChannel glChHandleSlFifoUtoP;   /* DMA Channel handle for U2P transfer. */
CyU3PDmaChannel glChHandleSlFifoPtoU;   /* DMA Channel handle for P2U transfer. */

uint32_t glDMARxCount = 0;               /* Counter to track the number of buffers received from USB. */
uint32_t glDMATxCount = 0;               /* Counter to track the number of buffers sent to USB. */
CyBool_t glIsApplnActive = CyFalse;      /* Whether the loopback application is active or not. */
CyBool_t fpgaConfModeActive = CyFalse;

/* Application Error Handler */
void
CyFxAppErrorHandler (
        CyU3PReturnStatus_t apiRetStatus    /* API return status */
        )
{
    /* Application failed with the error code apiRetStatus */

	CyU3PDebugPrint (6, "Fatal error %d occurred\n\r", apiRetStatus);

    /* Loop Indefinitely */
    for (;;)
    {
        /* Thread sleep : 100 ms */
        CyU3PThreadSleep (100);
    }
}

/* This function initializes the debug module. The debug prints
 * are routed to the UART and can be seen using a UART console
 * running at 115200 baud rate. */
#ifndef _USB_DEBUG_
	void
	CyFxSlFifoApplnDebugInit (void)
	{
		CyU3PUartConfig_t uartConfig;
		CyU3PReturnStatus_t apiRetStatus = CY_U3P_SUCCESS;

		/* Initialize the UART for printing debug messages */
		apiRetStatus = CyU3PUartInit();
		if (apiRetStatus != CY_U3P_SUCCESS)
		{
			/* Error handling */
			CyFxAppErrorHandler(apiRetStatus);
		}

		/* Set UART configuration */
		CyU3PMemSet ((uint8_t *)&uartConfig, 0, sizeof (uartConfig));
		uartConfig.baudRate = CY_U3P_UART_BAUDRATE_115200;
		uartConfig.stopBit = CY_U3P_UART_ONE_STOP_BIT;
		uartConfig.parity = CY_U3P_UART_NO_PARITY;
		uartConfig.txEnable = CyTrue;
		uartConfig.rxEnable = CyFalse;
		uartConfig.flowCtrl = CyFalse;
		uartConfig.isDma = CyTrue;

		apiRetStatus = CyU3PUartSetConfig (&uartConfig, NULL);
		if (apiRetStatus != CY_U3P_SUCCESS)
		{
			CyFxAppErrorHandler(apiRetStatus);
		}

		/* Set the UART transfer to a really large value. */
		apiRetStatus = CyU3PUartTxSetBlockXfer (0xFFFFFFFF);
		if (apiRetStatus != CY_U3P_SUCCESS)
		{
			CyFxAppErrorHandler(apiRetStatus);
		}

		/* Initialize the debug module. */
		apiRetStatus = CyU3PDebugInit (CY_U3P_LPP_SOCKET_UART_CONS, 8);
		if (apiRetStatus != CY_U3P_SUCCESS)
		{
			CyFxAppErrorHandler(apiRetStatus);
		}
	}
#endif

/* DMA callback function to handle the produce events for U to P transfers.
 * It's not used because DMA channel is normally in AUTO mode. Switch to MANUAL mode to enable this callback.
 */

void
CyFxSlFifoUtoPDmaCallback (
        CyU3PDmaChannel   *chHandle,
        CyU3PDmaCbType_t  type,
        CyU3PDmaCBInput_t *input
        )
{
    CyU3PReturnStatus_t status = CY_U3P_SUCCESS;
    //CyU3PDebugPrint (4, "UtoP DMA callback 0x%x\n\r", type);
    if (type == CY_U3P_DMA_CB_PROD_EVENT)
    {
        /* This is a produce event notification to the CPU. This notification is 
         * received upon reception of every buffer. The buffer will not be sent
         * out unless it is explicitly committed. The call shall fail if there
         * is a bus reset / usb disconnect or if there is any application error. */
        status = CyU3PDmaChannelCommitBuffer (chHandle, input->buffer_p.count, 0);
        if (status != CY_U3P_SUCCESS)
        {
            CyU3PDebugPrint (4, "CyU3PDmaChannelCommitBuffer failed, Error code = %d\n", status);
        }

    }
}

/* DMA callback function to handle the produce events for P to U transfers.
*  It's not used because DMA channel is normally in AUTO mode. Switch to MANUAL mode to enable this callback.
*/
void
CyFxSlFifoPtoUDmaCallback (
        CyU3PDmaChannel   *chHandle,
        CyU3PDmaCbType_t  type,
        CyU3PDmaCBInput_t *input
        )
{
    CyU3PReturnStatus_t status = CY_U3P_SUCCESS;
    //CyU3PDebugPrint (4, "PtoU DMA callback 0x%x\n\r", type);
    if (type == CY_U3P_DMA_CB_PROD_EVENT)
    {
        /* This is a produce event notification to the CPU. This notification is 
         * received upon reception of every buffer. The buffer will not be sent
         * out unless it is explicitly committed. The call shall fail if there
         * is a bus reset / usb disconnect or if there is any application error. */
        status = CyU3PDmaChannelCommitBuffer (chHandle, input->buffer_p.count, 0);
        if (status != CY_U3P_SUCCESS)
        {
            CyU3PDebugPrint (4, "CyU3PDmaChannelCommitBuffer failed, Error code = %d\n", status);
        }

    }
}

/* This function starts the application. This is called
 * when a SET_CONF event is received from the USB host. The endpoints
 * are configured and the DMA pipe is setup in this function. */
void
CyFxSlFifoApplnStart (
        void)
{
    uint16_t size = 0;
    CyU3PEpConfig_t epCfg;
    CyU3PDmaChannelConfig_t dmaCfg;
    CyU3PReturnStatus_t apiRetStatus = CY_U3P_SUCCESS;
    CyU3PUSBSpeed_t usbSpeed = CyU3PUsbGetSpeed();

    /* First identify the usb speed. Once that is identified,
     * create a DMA channel and start the transfer on this. */

    /* Based on the Bus Speed configure the endpoint packet size */
    switch (usbSpeed)
    {
        case CY_U3P_FULL_SPEED:
            size = 64;
            break;

        case CY_U3P_HIGH_SPEED:
            size = 512;
            break;

        case  CY_U3P_SUPER_SPEED:
            size = 1024;
            break;

        default:
            CyU3PDebugPrint (4, "Error! Invalid USB speed.\n");
            CyFxAppErrorHandler (CY_U3P_ERROR_FAILURE);
            break;
    }
#ifdef _USB_DEBUG_
    /* Debug endpoint configuration */
        CyU3PMemSet ((uint8_t *)&epCfg, 0, sizeof (epCfg));
        epCfg.enable = CyTrue;
        epCfg.epType = CY_U3P_USB_EP_INTR;
        epCfg.burstLen = 1;
        epCfg.streams = 0;
        epCfg.pcktSize = size;

        apiRetStatus = CyU3PSetEpConfig(CY_FX_EP_DEBUG, &epCfg);
        if (apiRetStatus != CY_U3P_SUCCESS)
        {
            CyFxAppErrorHandler (apiRetStatus);
        }

        /* Flush the Endpoint memory */
        CyU3PUsbFlushEp(CY_FX_EP_DEBUG);

        apiRetStatus = CyU3PDebugInit (CY_FX_EP_DEBUG_SOCKET, 8);
        if (apiRetStatus != CY_U3P_SUCCESS)
        {
            CyFxAppErrorHandler (apiRetStatus);
        }
#endif
    CyU3PMemSet ((uint8_t *)&epCfg, 0, sizeof (epCfg));
    epCfg.enable = CyTrue;
    epCfg.epType = CY_U3P_USB_EP_BULK;
    epCfg.burstLen = 1;
    epCfg.streams = 0;
    epCfg.pcktSize = size;

    /* Producer endpoint configuration */
    apiRetStatus = CyU3PSetEpConfig(CY_FX_EP_PRODUCER, &epCfg);
    if (apiRetStatus != CY_U3P_SUCCESS)
    {
        CyU3PDebugPrint (4, "CyU3PSetEpConfig failed, Error code = %d\n", apiRetStatus);
        CyFxAppErrorHandler (apiRetStatus);
    }

    /* Consumer endpoint configuration */
    apiRetStatus = CyU3PSetEpConfig(CY_FX_EP_CONSUMER, &epCfg);
    if (apiRetStatus != CY_U3P_SUCCESS)
    {
        CyU3PDebugPrint (4, "CyU3PSetEpConfig failed, Error code = %d\n", apiRetStatus);
        CyFxAppErrorHandler (apiRetStatus);
    }

    CyU3PUsbSetEpPktMode ( CY_FX_EP_PRODUCER,CyTrue);

    /* Create a DMA MANUAL channel for U2P transfer.
     * DMA size is set based on the USB speed. */
    dmaCfg.size  = size*SLFIFO_U2P_DMA_BUF_SIZE_MULT;
    dmaCfg.count = SLFIFO_DMA_BUF_COUNT;
    dmaCfg.prodSckId = CY_FX_PRODUCER_USB_SOCKET;
    dmaCfg.consSckId = CY_FX_CONSUMER_PPORT_SOCKET;
    dmaCfg.dmaMode = CY_U3P_DMA_MODE_BYTE;
    /* Enabling the callback for produce event. */
    dmaCfg.notification = CY_U3P_DMA_CB_PROD_EVENT;
    dmaCfg.cb = CyFxSlFifoUtoPDmaCallback;
    dmaCfg.prodHeader = 0;
    dmaCfg.prodFooter = 0;
    dmaCfg.consHeader = 0;
    dmaCfg.prodAvailCount = 0;

    apiRetStatus = CyU3PDmaChannelCreate (&glChHandleSlFifoUtoP,
            CY_U3P_DMA_TYPE_AUTO, &dmaCfg);
    if (apiRetStatus != CY_U3P_SUCCESS)
    {
        CyU3PDebugPrint (4, "CyU3PDmaChannelCreate failed, Error code = %d\n", apiRetStatus);
        CyFxAppErrorHandler(apiRetStatus);
    }

    /* Create a DMA MANUAL channel for P2U transfer. */
    dmaCfg.size  = size*SLFIFO_P2U_DMA_BUF_SIZE_MULT;
    dmaCfg.prodSckId = CY_FX_PRODUCER_PPORT_SOCKET;
    dmaCfg.consSckId = CY_FX_CONSUMER_USB_SOCKET;
    dmaCfg.cb = CyFxSlFifoPtoUDmaCallback;
    apiRetStatus = CyU3PDmaChannelCreate (&glChHandleSlFifoPtoU,
            CY_U3P_DMA_TYPE_AUTO, &dmaCfg);
    if (apiRetStatus != CY_U3P_SUCCESS)
    {
        CyU3PDebugPrint (4, "CyU3PDmaChannelCreate failed, Error code = %d\n", apiRetStatus);
        CyFxAppErrorHandler(apiRetStatus);
    }

    /* Flush the Endpoint memory */
    CyU3PUsbFlushEp(CY_FX_EP_PRODUCER);
    CyU3PUsbFlushEp(CY_FX_EP_CONSUMER);

    /* Set DMA channel transfer size. */
    apiRetStatus = CyU3PDmaChannelSetXfer (&glChHandleSlFifoUtoP, 0);
    if (apiRetStatus != CY_U3P_SUCCESS)
    {
        CyU3PDebugPrint (4, "CyU3PDmaChannelSetXfer Failed, Error code = %d\n", apiRetStatus);
        CyFxAppErrorHandler(apiRetStatus);
    }
    apiRetStatus = CyU3PDmaChannelSetXfer (&glChHandleSlFifoPtoU, 0);
    if (apiRetStatus != CY_U3P_SUCCESS)
    {
        CyU3PDebugPrint (4, "CyU3PDmaChannelSetXfer Failed, Error code = %d\n", apiRetStatus);
        CyFxAppErrorHandler(apiRetStatus);
    }

    /* Update the status flag. */
    glIsApplnActive = CyTrue;
}

/* This function stops the application. This shall be called
 * whenever a RESET or DISCONNECT event is received from the USB host. The
 * endpoints are disabled and the DMA pipe is destroyed by this function. */
void
CyFxSlFifoApplnStop (
        void)
{
    CyU3PEpConfig_t epCfg;
    CyU3PReturnStatus_t apiRetStatus = CY_U3P_SUCCESS;

    /* Update the flag. */
    glIsApplnActive = CyFalse;

    /* Flush the endpoint memory */
    CyU3PUsbFlushEp(CY_FX_EP_PRODUCER);
    CyU3PUsbFlushEp(CY_FX_EP_CONSUMER);

    /* Destroy the channel */
    CyU3PDmaChannelDestroy (&glChHandleSlFifoUtoP);
    CyU3PDmaChannelDestroy (&glChHandleSlFifoPtoU);

    /* Disable endpoints. */
    CyU3PMemSet ((uint8_t *)&epCfg, 0, sizeof (epCfg));
    epCfg.enable = CyFalse;

    /* Producer endpoint configuration. */
    apiRetStatus = CyU3PSetEpConfig(CY_FX_EP_PRODUCER, &epCfg);
    if (apiRetStatus != CY_U3P_SUCCESS)
    {
        CyU3PDebugPrint (4, "CyU3PSetEpConfig failed, Error code = %d\n", apiRetStatus);
        CyFxAppErrorHandler (apiRetStatus);
    }

    /* Consumer endpoint configuration. */
    apiRetStatus = CyU3PSetEpConfig(CY_FX_EP_CONSUMER, &epCfg);
    if (apiRetStatus != CY_U3P_SUCCESS)
    {
        CyU3PDebugPrint (4, "CyU3PSetEpConfig failed, Error code = %d\n", apiRetStatus);
        CyFxAppErrorHandler (apiRetStatus);
    }
#ifdef _USB_DEBUG_
    /* Disable the debug log mechanism. */
    CyU3PDebugDeInit ();

    /* Flush the endpoint memory */
    CyU3PUsbFlushEp(CY_FX_EP_DEBUG);

    /* Consumer endpoint configuration. */
    apiRetStatus = CyU3PSetEpConfig(CY_FX_EP_DEBUG, &epCfg);
    if (apiRetStatus != CY_U3P_SUCCESS)
    {
        CyFxAppErrorHandler (apiRetStatus);
    }
#endif
}

/* Callback to handle the USB setup requests. */
CyBool_t
CyFxSlFifoApplnUSBSetupCB (
        uint32_t setupdat0,
        uint32_t setupdat1
    )
{
    /* Fast enumeration is used. Only requests addressed to the interface, class,
     * vendor and unknown control requests are received by this function.
     * This application does not support any class or vendor requests. */

    uint8_t  bRequest, bReqType;
    uint8_t  bType, bTarget;
    uint16_t wValue, wIndex, wLength;
    CyBool_t isHandled = CyFalse;
    CyU3PReturnStatus_t status = CY_U3P_SUCCESS;

    CyBool_t nStatus, ConfDone;
    uint32_t FirmwareLen;

    /* Decode the fields from the setup request. */
    bReqType = (setupdat0 & CY_U3P_USB_REQUEST_TYPE_MASK);
    bType    = (bReqType & CY_U3P_USB_TYPE_MASK);
    bTarget  = (bReqType & CY_U3P_USB_TARGET_MASK);
    bRequest = ((setupdat0 & CY_U3P_USB_REQUEST_MASK) >> CY_U3P_USB_REQUEST_POS);
    wValue   = ((setupdat0 & CY_U3P_USB_VALUE_MASK)   >> CY_U3P_USB_VALUE_POS);
    wIndex   = ((setupdat1 & CY_U3P_USB_INDEX_MASK)   >> CY_U3P_USB_INDEX_POS);
    wLength   = ((setupdat1 & CY_U3P_USB_LENGTH_MASK)   >> CY_U3P_USB_LENGTH_POS);

    if (bType == CY_U3P_USB_STANDARD_RQT)
    {
        /* Handle SET_FEATURE(FUNCTION_SUSPEND) and CLEAR_FEATURE(FUNCTION_SUSPEND)
         * requests here. It should be allowed to pass if the device is in configured
         * state and failed otherwise. */
        if ((bTarget == CY_U3P_USB_TARGET_INTF) && ((bRequest == CY_U3P_USB_SC_SET_FEATURE)
                    || (bRequest == CY_U3P_USB_SC_CLEAR_FEATURE)) && (wValue == 0))
        {
            if (glIsApplnActive)
                CyU3PUsbAckSetup ();
            else
                CyU3PUsbStall (0, CyTrue, CyFalse);

            isHandled = CyTrue;
        }

        /* CLEAR_FEATURE request for endpoint is always passed to the setup callback
         * regardless of the enumeration model used. When a clear feature is received,
         * the previous transfer has to be flushed and cleaned up. This is done at the
         * protocol level. Since this is just a loopback operation, there is no higher
         * level protocol. So flush the EP memory and reset the DMA channel associated
         * with it. If there are more than one EP associated with the channel reset both
         * the EPs. The endpoint stall and toggle / sequence number is also expected to be
         * reset. Return CyFalse to make the library clear the stall and reset the endpoint
         * toggle. Or invoke the CyU3PUsbStall (ep, CyFalse, CyTrue) and return CyTrue.
         * Here we are clearing the stall. */
        if ((bTarget == CY_U3P_USB_TARGET_ENDPT) && (bRequest == CY_U3P_USB_SC_CLEAR_FEATURE)
                && (wValue == CY_U3P_USBX_FS_EP_HALT))
        {
            if (glIsApplnActive)
            {
                if (wIndex == CY_FX_EP_PRODUCER)
                {
                    CyU3PUsbSetEpNak (CY_FX_EP_PRODUCER, CyTrue);
                    CyU3PBusyWait (125);

                    CyU3PDmaChannelReset (&glChHandleSlFifoUtoP);
                    CyU3PUsbFlushEp(CY_FX_EP_PRODUCER);
                    CyU3PUsbResetEp (CY_FX_EP_PRODUCER);
                    CyU3PDmaChannelSetXfer (&glChHandleSlFifoUtoP, 0);

                    CyU3PUsbSetEpNak (CY_FX_EP_PRODUCER, CyFalse);
                }

                if (wIndex == CY_FX_EP_CONSUMER)
                {
                    CyU3PUsbSetEpNak (CY_FX_EP_PRODUCER, CyTrue);
                    CyU3PBusyWait (125);

                    CyU3PDmaChannelReset (&glChHandleSlFifoPtoU);
                    CyU3PUsbFlushEp(CY_FX_EP_CONSUMER);
                    CyU3PUsbResetEp (CY_FX_EP_CONSUMER);
                    CyU3PDmaChannelSetXfer (&glChHandleSlFifoPtoU, 0);

                    CyU3PUsbSetEpNak (CY_FX_EP_PRODUCER, CyFalse);
                }

                if (wIndex == CY_FX_EP_DEBUG)
                {
                	CyU3PDebugDeInit ();
                	CyU3PUsbFlushEp(CY_FX_EP_DEBUG);
                	CyU3PDebugInit(CY_FX_EP_DEBUG_SOCKET, 8);
                }

                CyU3PUsbStall (wIndex, CyFalse, CyTrue);

                CyU3PUsbAckSetup ();
                isHandled = CyTrue;
            }
        }
    }
    /* Handle supported vendor requests. */
    else if (bType == CY_U3P_USB_VENDOR_RQT)
    {
    	isHandled = CyTrue;
    	switch (bRequest)
    	{
    		case RQT_ID_CHECK:
    			CyU3PUsbSendEP0Data (8, (uint8_t *)glFirmwareID);
    			//CyU3PDebugLog (1, 0xFF, 0x67);
    			//CyU3PDebugLogFlush();
    	        break;

    	    case RQT_I2C_WRITE:
    	        status  = CyU3PUsbGetEP0Data(wLength, glEp0Buffer, NULL);
    	        if (status == CY_U3P_SUCCESS)
    	        {
    	        	CyFxUsbI2cTransfer ( ((uint32_t)(wValue)<<16) | ((uint32_t)wIndex),
    	        			FPGA_I2C_DEV_ADDR, wLength, glEp0Buffer, EP0_BUFFER_SIZE, CyFalse);
    	        }
    	        break;

    	    case RQT_I2C_READ:
    	        CyU3PMemSet (glEp0Buffer, 0, sizeof (glEp0Buffer));
    	        status = CyFxUsbI2cTransfer ( ((uint32_t)(wValue)<<16) | ((uint32_t)wIndex),
    	        		FPGA_I2C_DEV_ADDR, wLength, glEp0Buffer, EP0_BUFFER_SIZE , CyTrue);
    	        if (status == CY_U3P_SUCCESS)
    	        {
    	        	status = CyU3PUsbSendEP0Data(wLength, glEp0Buffer);
    	        }
    	        break;

    	    case RQT_FPGA_CONFIG_MODE_ON:
    	    	/*
    	    	 * To send a configuration data into FPGA a 32-bit FPP mode is used.
    	    	 * In order to perform the FPGA configuration, the GPIF interface must be reconfigured
    	    	 * to an asynchronous master FIFO mode with an artificial clock signal supplied
    	    	 * from the GPIO 26 to the DCLK pin of the FPGA. The GPIO 26 is controlled by a GPIF state
    	    	 * machine.
    	    	 */
    	    	CyU3PUsbAckSetup ();
    	    	fpgaConfModeActive = CyTrue;
    	    	CyU3PGpifDisable(CyTrue);

    	    	/*
    	    	 * Initialize the GPIF interface in an asynchronous master FIFO mode with the
    	    	 * configuration from gpig_master_config.h
    	    	 * */
    	    	GpifMasterInit();

    	    	/* Initialize FPGA reconfiguration */
    	    	CyU3PGpioSetValue (GPIO_nCONFIG, CyFalse);
    	    	CyU3PThreadSleep (1);
    	    	CyU3PGpioSetValue (GPIO_nCONFIG, CyTrue);
    	    	CyU3PThreadSleep (1);

    	    	/* Check FPGA status signals */
    	    	CyU3PGpioGetValue (GPIO_nSTAT, &nStatus);
    	    	CyU3PGpioGetValue (GPIO_CONF_DONE, &ConfDone);
    	    	glEp0Buffer[0] = nStatus;
    	    	glEp0Buffer[1] = ConfDone;
    	    	FirmwareLen = ((uint32_t)(wValue)<<16) | (uint32_t)wIndex;
    	    	CyU3PGpifInitDataCounter(0, FirmwareLen, CyFalse, CyTrue, 1);
    	    	CyU3PUsbSendEP0Data (2, glEp0Buffer);

    	    	break;

    	    case RQT_FPGA_CONFIG_CHECK:
    	    	CyU3PGpioGetValue (GPIO_nSTAT, &nStatus);
    	    	CyU3PGpioGetValue (GPIO_CONF_DONE, &ConfDone);
    	    	glEp0Buffer[0] = nStatus;
    	    	glEp0Buffer[1] = ConfDone;
    	    	CyU3PUsbSendEP0Data (2, glEp0Buffer);
    	    	break;

    	    case RQT_FPGA_CONFIG_MODE_OFF:
    	    	/*
    	    	 * To exit from the FPGA configuration mode, the GPIF interface must me reconfigured back
    	    	 * to an synchronous slave FIFO mode.
    	    	 */
    	    	CyU3PUsbAckSetup ();
    	    	fpgaConfModeActive = CyFalse;
    	    	CyU3PGpifDisable(CyTrue);
    	    	GpifSlaveInit();

    	    	CyU3PThreadSleep (1);
    	    	CyU3PGpioGetValue (GPIO_nSTAT, &nStatus);
    	    	CyU3PGpioGetValue (GPIO_CONF_DONE, &ConfDone);
    	    	glEp0Buffer[0] = nStatus;
    	    	glEp0Buffer[1] = ConfDone;
    	    	CyU3PUsbSendEP0Data (2, glEp0Buffer);
    	    	break;

    	    case RQT_I2C_RESET:
    	    	CyU3PUsbAckSetup ();
    	    	I2C_DMA_Reset();
    	    	break;

    	    case RQT_RESET:
    	        {
    	        	CyU3PUsbAckSetup ();
    	            CyU3PThreadSleep (1000);
    	            CyU3PDeviceReset (CyFalse);
    	        }
    	        break;

    	    default:
    	         /* This is unknown request. */
    	    	isHandled = CyFalse;
    	        break;
    	}

    	/* If there was any error, return not handled so that the library will
    	 * stall the request. Alternatively EP0 can be stalled here and return
    	 * CyTrue. */
    	 if (status != CY_U3P_SUCCESS)
    	 {
    		 isHandled = CyFalse;
    	 }
    }

    return isHandled;
}

/* This is the callback function to handle the USB events. */
void
CyFxSlFifoApplnUSBEventCB (
    CyU3PUsbEventType_t evtype,
    uint16_t            evdata
    )
{
    switch (evtype)
    {
        case CY_U3P_USB_EVENT_SETCONF:
        	CyU3PDebugPrint (6, "Setconfig event\n\r");
            /* Disable the low power entry to optimize USB throughput */
            CyU3PUsbLPMDisable();
            /* Stop the application before re-starting. */
            if (glIsApplnActive)
            {
            	CyU3PDebugPrint (6, "glIsApplnActive is True\n\r");
                CyFxSlFifoApplnStop ();
                I2C_DMA_Reset();
            }
            /* Start the loop back function. */
            CyFxSlFifoApplnStart ();
            break;

        case CY_U3P_USB_EVENT_RESET:
        	CyU3PDebugPrint (6, "Reset event\n\r");
        	CyU3PDeviceReset (CyFalse);
        	break;
        case CY_U3P_USB_EVENT_DISCONNECT:
            /* Stop the loop back function. */
        	CyU3PDebugPrint (6, "Disconnect event\n\r");
            if (glIsApplnActive)
            {
            	CyU3PDebugPrint (6, "glIsApplnActive is True\n\r");
                CyFxSlFifoApplnStop ();
                I2C_DMA_Reset();
            }
            break;

        default:
            break;
    }
}

/* Callback function to handle LPM requests from the USB 3.0 host. This function is invoked by the API
   whenever a state change from U0 -> U1 or U0 -> U2 happens. If we return CyTrue from this function, the
   FX3 device is retained in the low power state. If we return CyFalse, the FX3 device immediately tries
   to trigger an exit back to U0.

   This application does not have any state in which we should not allow U1/U2 transitions; and therefore
   the function always return CyTrue.
 */
CyBool_t
CyFxApplnLPMRqtCB (
        CyU3PUsbLinkPowerMode link_mode)
{
    return CyTrue;
}

/* This function initializes the GPIF interface and initializes
 * the USB interface. */
void
CyFxSlFifoApplnInit (void)
{
    CyU3PPibClock_t pibClock;
    CyU3PReturnStatus_t apiRetStatus = CY_U3P_SUCCESS;

    CyU3PGpioClock_t gpioClock;
    CyU3PGpioSimpleConfig_t gpioOutConf, gpioInConf;


    /* Initialize the p-port block. */
    pibClock.clkDiv = 2;
    pibClock.clkSrc = CY_U3P_SYS_CLK;
    pibClock.isHalfDiv = CyFalse;
    /* Disable DLL for sync GPIF */
    pibClock.isDllEnable = CyFalse;
    apiRetStatus = CyU3PPibInit(CyTrue, &pibClock);
    if (apiRetStatus != CY_U3P_SUCCESS)
    {
        CyU3PDebugPrint (4, "P-port Initialization failed, Error Code = %d\n\r",apiRetStatus);
        CyFxAppErrorHandler(apiRetStatus);
    }

    GpifSlaveInit();
    CyU3PDebugPrint (6, "GPIF slave init\n\r");

    /* Init the GPIO module */
    gpioClock.fastClkDiv = 2;
    gpioClock.slowClkDiv = 0;
    gpioClock.simpleDiv = CY_U3P_GPIO_SIMPLE_DIV_BY_2;
    gpioClock.clkSrc = CY_U3P_SYS_CLK;
    gpioClock.halfDiv = 0;

    apiRetStatus = CyU3PGpioInit(&gpioClock, NULL);
    if (apiRetStatus != 0)
    {
    	/* Error Handling */
    	CyU3PDebugPrint (4, "CyU3PGpioInit failed, error code = %d\n", apiRetStatus);
    	CyFxAppErrorHandler(apiRetStatus);
    }

    /* GPIO Out config */
    gpioOutConf.outValue = CyTrue;
    gpioOutConf.driveLowEn = CyTrue;
    gpioOutConf.driveHighEn = CyTrue;
    gpioOutConf.inputEn = CyFalse;
    gpioOutConf.intrMode = CY_U3P_GPIO_NO_INTR;
    /* GPIO In config */
    gpioInConf.outValue = CyFalse;
    gpioInConf.driveLowEn = CyFalse;
    gpioInConf.driveHighEn = CyFalse;
    gpioInConf.inputEn = CyTrue;
    gpioInConf.intrMode = CY_U3P_GPIO_NO_INTR;

    /* Configure GPIO nCONFIG output */
    /*
    apiRetStatus = CyU3PGpioSetSimpleConfig(26, &gpioOutConf);
    if (apiRetStatus != CY_U3P_SUCCESS)
    {
        CyU3PDebugPrint (4, "CyU3PGpioSetSimpleConfig failed %d, error code = %d\n",26,apiRetStatus);
        CyFxAppErrorHandler(apiRetStatus);
    }*/

    apiRetStatus = CyU3PGpioSetSimpleConfig(GPIO_nCONFIG, &gpioOutConf);
    if (apiRetStatus != CY_U3P_SUCCESS)
    {
         CyU3PDebugPrint (4, "CyU3PGpioSetSimpleConfig failed %d, error code = %d\n",51,apiRetStatus);
         CyFxAppErrorHandler(apiRetStatus);
     }

    /* Configure GPIO CONF_DONE input */
    apiRetStatus = CyU3PGpioSetSimpleConfig(GPIO_CONF_DONE, &gpioInConf);
    if (apiRetStatus != CY_U3P_SUCCESS)
    {
    	CyU3PDebugPrint (4, "CyU3PGpioSetSimpleConfig failed %d, error code = %d\n",50,apiRetStatus);
      	CyFxAppErrorHandler(apiRetStatus);
    }
    /* Configure nSTAT input */
    apiRetStatus = CyU3PGpioSetSimpleConfig(GPIO_nSTAT, &gpioInConf);
    if (apiRetStatus != CY_U3P_SUCCESS)
    {
        CyU3PDebugPrint (4, "CyU3PGpioSetSimpleConfig failed %d, error code = %d\n",52,apiRetStatus);
        CyFxAppErrorHandler(apiRetStatus);
    }

    /* Start the USB functionality. */
    apiRetStatus = CyU3PUsbStart();
    if (apiRetStatus != CY_U3P_SUCCESS)
    {
        CyU3PDebugPrint (4, "CyU3PUsbStart failed to Start, Error code = %d\n", apiRetStatus);
        CyFxAppErrorHandler(apiRetStatus);
    }

    /* The fast enumeration is the easiest way to setup a USB connection,
     * where all enumeration phase is handled by the library. Only the
     * class / vendor requests need to be handled by the application. */
    CyU3PUsbRegisterSetupCallback(CyFxSlFifoApplnUSBSetupCB, CyTrue);

    /* Setup the callback to handle the USB events. */
    CyU3PUsbRegisterEventCallback(CyFxSlFifoApplnUSBEventCB);

    /* Register a callback to handle LPM requests from the USB 3.0 host. */
    CyU3PUsbRegisterLPMRequestCallback(CyFxApplnLPMRqtCB);

    /* Set the USB Enumeration descriptors */

    /* Super speed device descriptor. */
    apiRetStatus = CyU3PUsbSetDesc(CY_U3P_USB_SET_SS_DEVICE_DESCR, 0, (uint8_t *)CyFxUSB30DeviceDscr);
    if (apiRetStatus != CY_U3P_SUCCESS)
    {
        CyU3PDebugPrint (4, "USB set device descriptor failed, Error code = %d\n", apiRetStatus);
        CyFxAppErrorHandler(apiRetStatus);
    }

    /* High speed device descriptor. */
    apiRetStatus = CyU3PUsbSetDesc(CY_U3P_USB_SET_HS_DEVICE_DESCR, 0, (uint8_t *)CyFxUSB20DeviceDscr);
    if (apiRetStatus != CY_U3P_SUCCESS)
    {
        CyU3PDebugPrint (4, "USB set device descriptor failed, Error code = %d\n", apiRetStatus);
        CyFxAppErrorHandler(apiRetStatus);
    }

    /* BOS descriptor */
    apiRetStatus = CyU3PUsbSetDesc(CY_U3P_USB_SET_SS_BOS_DESCR, 0, (uint8_t *)CyFxUSBBOSDscr);
    if (apiRetStatus != CY_U3P_SUCCESS)
    {
        CyU3PDebugPrint (4, "USB set configuration descriptor failed, Error code = %d\n", apiRetStatus);
        CyFxAppErrorHandler(apiRetStatus);
    }

    /* Device qualifier descriptor */
    apiRetStatus = CyU3PUsbSetDesc(CY_U3P_USB_SET_DEVQUAL_DESCR, 0, (uint8_t *)CyFxUSBDeviceQualDscr);
    if (apiRetStatus != CY_U3P_SUCCESS)
    {
        CyU3PDebugPrint (4, "USB set device qualifier descriptor failed, Error code = %d\n", apiRetStatus);
        CyFxAppErrorHandler(apiRetStatus);
    }

    /* Super speed configuration descriptor */
    apiRetStatus = CyU3PUsbSetDesc(CY_U3P_USB_SET_SS_CONFIG_DESCR, 0, (uint8_t *)CyFxUSBSSConfigDscr);
    if (apiRetStatus != CY_U3P_SUCCESS)
    {
        CyU3PDebugPrint (4, "USB set configuration descriptor failed, Error code = %d\n", apiRetStatus);
        CyFxAppErrorHandler(apiRetStatus);
    }

    /* High speed configuration descriptor */
    apiRetStatus = CyU3PUsbSetDesc(CY_U3P_USB_SET_HS_CONFIG_DESCR, 0, (uint8_t *)CyFxUSBHSConfigDscr);
    if (apiRetStatus != CY_U3P_SUCCESS)
    {
        CyU3PDebugPrint (4, "USB Set Other Speed Descriptor failed, Error Code = %d\n", apiRetStatus);
        CyFxAppErrorHandler(apiRetStatus);
    }

    /* Full speed configuration descriptor */
    apiRetStatus = CyU3PUsbSetDesc(CY_U3P_USB_SET_FS_CONFIG_DESCR, 0, (uint8_t *)CyFxUSBFSConfigDscr);
    if (apiRetStatus != CY_U3P_SUCCESS)
    {
        CyU3PDebugPrint (4, "USB Set Configuration Descriptor failed, Error Code = %d\n", apiRetStatus);
        CyFxAppErrorHandler(apiRetStatus);
    }

    /* String descriptor 0 */
    apiRetStatus = CyU3PUsbSetDesc(CY_U3P_USB_SET_STRING_DESCR, 0, (uint8_t *)CyFxUSBStringLangIDDscr);
    if (apiRetStatus != CY_U3P_SUCCESS)
    {
        CyU3PDebugPrint (4, "USB set string descriptor 0 failed, Error code = %d\n", apiRetStatus);
        CyFxAppErrorHandler(apiRetStatus);
    }

    /* String descriptor 1 */
    apiRetStatus = CyU3PUsbSetDesc(CY_U3P_USB_SET_STRING_DESCR, 1, (uint8_t *)CyFxUSBManufactureDscr);
    if (apiRetStatus != CY_U3P_SUCCESS)
    {
        CyU3PDebugPrint (4, "USB set string descriptor 1 failed, Error code = %d\n", apiRetStatus);
        CyFxAppErrorHandler(apiRetStatus);
    }

    /* String descriptor 2 */
    apiRetStatus = CyU3PUsbSetDesc(CY_U3P_USB_SET_STRING_DESCR, 2, (uint8_t *)CyFxUSBProductDscr);
    if (apiRetStatus != CY_U3P_SUCCESS)
    {
        CyU3PDebugPrint (4, "USB set string descriptor 2 failed, Error code = %d\n", apiRetStatus);
        CyFxAppErrorHandler(apiRetStatus);
    }

    /* String descriptor 3 Serial number*/
    apiRetStatus = CyU3PUsbSetDesc(CY_U3P_USB_SET_STRING_DESCR, 3, MkSerialStringDescriptor());
    if (apiRetStatus != CY_U3P_SUCCESS)
    {
         CyU3PDebugPrint (4, "USB set string descriptor 3 failed, Error code = %d\n", apiRetStatus);
         CyFxAppErrorHandler(apiRetStatus);
     }

    /* Connect the USB Pins with super speed operation enabled. */
    apiRetStatus = CyU3PConnectState(CyTrue, CyTrue);
    if (apiRetStatus != CY_U3P_SUCCESS)
    {
        CyU3PDebugPrint (4, "USB Connect failed, Error code = %d\n", apiRetStatus);
        CyFxAppErrorHandler(apiRetStatus);
    }

}

/* Entry function for the slFifoAppThread. */
void
SlFifoAppThread_Entry (uint32_t input)
{
	CyU3PReturnStatus_t status = CY_U3P_SUCCESS;
	uint8_t state;

    /* Initialize the debug module */
#ifndef _USB_DEBUG_
    CyFxSlFifoApplnDebugInit();
    CyU3PDebugPrint (6, "UART Debug initialized\n\r");
#endif
    /* Initialize the I2C application */
    status = CyFxUsbI2cInit();
#ifndef _USB_DEBUG
    if(status != CY_U3P_SUCCESS)
    {
    	CyU3PDebugPrint (6, "I2C initialization failed, status %d\n\r", status);
	}
#endif
    /* Initialize the slave FIFO application */
    CyFxSlFifoApplnInit();
#ifndef _USB_DEBUG_
    CyU3PDebugPrint (6, "Application initialized\n\r");
#endif
    for (;;)
    {
        CyU3PThreadSleep (1000);
        if (fpgaConfModeActive)
        {
            /* Print the number of buffers received so far from the USB host. */
        	CyU3PGpifGetSMState (&state);
            CyU3PDebugPrint (6, "Master SM state: %d\n\r", state );
        }
    }
}

/* Application define function which creates the threads. */
void
CyFxApplicationDefine (
        void)
{
    void *ptr = NULL;
    uint32_t retThrdCreate = CY_U3P_SUCCESS;

    /* Allocate the memory for the thread */
    ptr = CyU3PMemAlloc (CY_FX_SLFIFO_THREAD_STACK);

    /* Create the thread for the application */
    retThrdCreate = CyU3PThreadCreate (&slFifoAppThread,           /* Slave FIFO app thread structure */
                          "21:Slave_FIFO_sync",                    /* Thread ID and thread name */
                          SlFifoAppThread_Entry,                   /* Slave FIFO app thread entry function */
                          0,                                       /* No input parameter to thread */
                          ptr,                                     /* Pointer to the allocated thread stack */
                          CY_FX_SLFIFO_THREAD_STACK,               /* App Thread stack size */
                          CY_FX_SLFIFO_THREAD_PRIORITY,            /* App Thread priority */
                          CY_FX_SLFIFO_THREAD_PRIORITY,            /* App Thread pre-emption threshold */
                          CYU3P_NO_TIME_SLICE,                     /* No time slice for the application thread */
                          CYU3P_AUTO_START                         /* Start the thread immediately */
                          );

    /* Check the return code */
    if (retThrdCreate != 0)
    {
        /* Thread Creation failed with the error code retThrdCreate */

        /* Add custom recovery or debug actions here */

        /* Application cannot continue */
        /* Loop indefinitely */
        while(1);
    }
}

/*
 * Main function
 */
int
main (void)
{
    CyU3PIoMatrixConfig_t io_cfg;
    CyU3PReturnStatus_t status = CY_U3P_SUCCESS;
    CyU3PSysClockConfig_t clockConfig;

    /* When the GPIF data bus is configured as 32-bits wide and running at 100 MHz (synchronous),
       the FX3 system clock has to be set to a frequency greater than 400 MHz. */
    clockConfig.setSysClk400  = CyTrue;
    clockConfig.cpuClkDiv     = 2;
    clockConfig.dmaClkDiv     = 2;
    clockConfig.mmioClkDiv    = 2;
    clockConfig.useStandbyClk = CyFalse;
    clockConfig.clkSrc        = CY_U3P_SYS_CLK;
    status = CyU3PDeviceInit (&clockConfig);
    if (status != CY_U3P_SUCCESS)
    {
        goto handle_fatal_error;
    }

    /* Initialize the caches. Enable both Instruction and Data Caches. */
    status = CyU3PDeviceCacheControl (CyTrue, CyTrue, CyTrue);
    if (status != CY_U3P_SUCCESS)
    {
        goto handle_fatal_error;
    }

    /* Configure the IO matrix for the device. */
    io_cfg.s0Mode = CY_U3P_SPORT_INACTIVE;
    io_cfg.s1Mode = CY_U3P_SPORT_INACTIVE;
    io_cfg.useUart   = CyTrue;
    io_cfg.useI2C    = CyTrue;
    io_cfg.useI2S    = CyFalse;
    io_cfg.useSpi    = CyFalse;
    io_cfg.isDQ32Bit = CyTrue;
    io_cfg.lppMode   = CY_U3P_IO_MATRIX_LPP_DEFAULT;
    /* Enable GPIO. */
    io_cfg.gpioSimpleEn[0]  = 0;
    io_cfg.gpioSimpleEn[1]  = 1<<(GPIO_CONF_DONE-32)| 1<<(GPIO_nCONFIG-32) | 1<<(GPIO_nSTAT-32);
    io_cfg.gpioComplexEn[0] = 0;
    io_cfg.gpioComplexEn[1] = 0;
    status = CyU3PDeviceConfigureIOMatrix (&io_cfg);
    if (status != CY_U3P_SUCCESS)
    {
        goto handle_fatal_error;
    }

    /* This is a non returnable call for initializing the RTOS kernel */
    CyU3PKernelEntry ();

    /* Dummy return to make the compiler happy */
    return 0;

handle_fatal_error:

    /* Cannot recover from this error. */
    while (1);
}

/* [ ] */

