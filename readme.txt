Qubit DAQ USB firmware for FX3 chip
 

  Files:

    * cyfx_gcc_startup.S   : Start-up code for the ARM-9 core on the FX3
      device.  This assembly source file follows the syntax for the GNU
      assembler.

    * cyfxslfifosync.h     : C header file that defines main constants

    * cyfxslfifousbdscr.c  : C source file that contains USB descriptors

    * gpif_slave_config.h    : C header file that contains the data required
      to configure the GPIF interface to implement the Sync. Slave FIFO
      protocol.

    * gpif_master_config.h    : C header file that contains the data required
      to configure the GPIF interface to implement the aSync. master FIFO
      protocol for configuration of the FPGA.

    * cyfxtx.c             : C source file that provides ThreadX RTOS wrapper
      functions and other utilites required by the FX3 firmware library.

    * cyfxslfifosync.c     : Main C source file.

    * makefile             : GNU make compliant build script for compiling

[]

