/* I2C related functionality
 */

#include "cyu3usb.h"
#include "cyu3i2c.h"
#include "usb_i2c.h"
#include "cyu3error.h"
#include "i2c_regs.h"

CyU3PDmaChannel glI2cTxHandle;   /* I2C Tx channel handle */
CyU3PDmaChannel glI2cRxHandle;   /* I2C Rx channel handle */

/* I2c initialization. */
CyU3PReturnStatus_t
CyFxUsbI2cInit (void)
{
    CyU3PI2cConfig_t 		i2cConfig;
    CyU3PDmaChannelConfig_t dmaConfig;
    CyU3PReturnStatus_t 	status = CY_U3P_SUCCESS;

    /* Initialize and configure the I2C master module. */
    status = CyU3PI2cInit ();
    if (status != CY_U3P_SUCCESS)
    {
    	CyU3PDebugPrint (6, "I2C initialization failed at CyU3PI2cInit, status %d\n\r", status);
        return status;
    }

    /* Start the I2C master block. The bit rate is set at 400KHz.
     * The data transfer is done via DMA. */
    CyU3PMemSet ((uint8_t *)&i2cConfig, 0, sizeof(i2cConfig));
    i2cConfig.bitRate    = CY_FX_USBI2C_I2C_BITRATE;
    i2cConfig.busTimeout = 0xFFFFFFFF;
    i2cConfig.dmaTimeout = 0xFFFF;
    i2cConfig.isDma      = CyTrue;

    status = CyU3PI2cSetConfig (&i2cConfig, NULL);
    if (status != CY_U3P_SUCCESS)
    {
    	CyU3PDebugPrint (6, "I2C initialization failed at CyU3PI2cSetConfig, status %d\n\r", status);
        return status;
    }

    /* Now create the DMA channels required for read and write. */
    CyU3PMemSet ((uint8_t *)&dmaConfig, 0, sizeof(dmaConfig));
    /* Size must be a multiple of 16*/
    dmaConfig.size           = 4096;
    /* No buffers need to be allocated as this will be used
     * only in override mode. */
    dmaConfig.count          = 0;
    dmaConfig.prodAvailCount = 0;
    dmaConfig.dmaMode        = CY_U3P_DMA_MODE_BYTE;
    dmaConfig.prodHeader     = 0;
    dmaConfig.prodFooter     = 0;
    dmaConfig.consHeader     = 0;
    dmaConfig.notification   = 0;
    dmaConfig.cb             = NULL;

    /* Create a channel to write to the I2C. */
    dmaConfig.prodSckId = CY_U3P_CPU_SOCKET_PROD;
    dmaConfig.consSckId = CY_U3P_LPP_SOCKET_I2C_CONS;
    status = CyU3PDmaChannelCreate (&glI2cTxHandle,
            CY_U3P_DMA_TYPE_MANUAL_OUT, &dmaConfig);
    if (status != CY_U3P_SUCCESS)
    {
    	CyU3PDebugPrint (6, "I2C initialization failed at write CyU3PDmaChannelCreate, status %d\n\r", status);
        return status;
    }

    /* Create a channel to read from the I2C. */
    dmaConfig.prodSckId = CY_U3P_LPP_SOCKET_I2C_PROD;
    dmaConfig.consSckId = CY_U3P_CPU_SOCKET_CONS;
    status = CyU3PDmaChannelCreate (&glI2cRxHandle,
            CY_U3P_DMA_TYPE_MANUAL_IN, &dmaConfig);

    if (status != CY_U3P_SUCCESS)
    {
    	CyU3PDebugPrint (6, "I2C initialization failed at read CyU3PDmaChannelCreate, status %d\n\r", status);
    }

    return status;
}

void
I2CStatusDbugPrint(void)
{
	CyU3PDebugPrint (6, "I2C registers \n\r");
	CyU3PDebugPrint (6, "Bytes transferred %d \n\r", I2C->lpp_i2c_bytes_transferred);
	CyU3PDebugPrint (6, "Byte count        %d \n\r", I2C->lpp_i2c_byte_count);
	CyU3PDebugPrint (6, "Clock low count   %d \n\r", I2C->lpp_i2c_clock_low_count);
	CyU3PDebugPrint (6, "Socket            0x%x \n\r", I2C->lpp_i2c_socket);
	CyU3PDebugPrint (6, "Power             0x%x \n\r", I2C->lpp_i2c_power);
	CyU3PDebugPrint (6, "Command           0x%x \n\r", I2C->lpp_i2c_command);
	CyU3PDebugPrint (6, "Config            0x%x \n\r", I2C->lpp_i2c_config);
	CyU3PDebugPrint (6, "Status            0x%x \n\r", I2C->lpp_i2c_status);
	CyU3PDebugPrint (6, "Preamble data0    0x%x \n\r", I2C->lpp_i2c_preamble_data0);
	CyU3PDebugPrint (6, "Preamble data1    0x%x \n\r", I2C->lpp_i2c_preamble_data1);
	CyU3PDebugPrint (6, "Preamble ctrl     0x%x \n\r", I2C->lpp_i2c_preamble_ctrl);
	CyU3PDebugPrint (6, "DMA timeout       %d \n\r", I2C->lpp_i2c_dma_timeout);
	CyU3PDebugPrint (6, "Timeout           %d \n\r", I2C->lpp_i2c_timeout);
}

void
BufferDebugPrint(uint8_t *buffer, uint8_t n)
{
	uint8_t i;

	for(i=0;i<n;i++)
	{
		CyU3PDebugPrint (6, "%d ", buffer[i]);
	}
	CyU3PDebugPrint (6, "\n\r");
}

/* I2C read / write. */
CyU3PReturnStatus_t
CyFxUsbI2cTransfer (
        uint32_t  byteAddress,
        uint8_t   devAddr,
        uint16_t  byteCount,
        uint8_t  *buffer,
        uint16_t  buffSize,
        CyBool_t  isRead)
{
    CyU3PDmaBuffer_t buf_p;
    CyU3PI2cPreamble_t preamble;
    CyU3PReturnStatus_t status = CY_U3P_SUCCESS;

    if (byteCount == 0)
    {
        return CY_U3P_SUCCESS;
    }

    CyU3PDebugPrint (2, "I2C access - dev: 0x%x, address: 0x%x, size: %d, read: %d.\n\r",
            devAddr, byteAddress, byteCount, (uint8_t)isRead);

    /* Update the buffer address. */
    buf_p.buffer = buffer;
    buf_p.status = 0;

	if (isRead)
	{
		/* Update the preamble information. */
		preamble.length    = 6;
		preamble.buffer[0] = devAddr<<1;
		preamble.buffer[1] = (uint8_t)(byteAddress >> 24);
		preamble.buffer[2] = (uint8_t)(byteAddress >> 16);
		preamble.buffer[3] = (uint8_t)(byteAddress >> 8);
		preamble.buffer[4] = (uint8_t)(byteAddress & 0xFF);
		preamble.buffer[5] = ((devAddr<<1) | 0x01);
		preamble.ctrlMask  = 1<<4;

		buf_p.size = buffSize;
		buf_p.count = byteCount;

		status = CyU3PI2cSendCommand (&preamble, byteCount, CyTrue);
		if (status != CY_U3P_SUCCESS)
		{
			CyU3PDebugPrint (6, "I2C read failed at CyU3PI2cSendCommand, status 0x%x\n\r", status);
			return status;
		}
		status = CyU3PDmaChannelSetupRecvBuffer (&glI2cRxHandle, &buf_p);
		if (status != CY_U3P_SUCCESS)
		{
			CyU3PDebugPrint (6, "I2C read failed at CyU3PDmaChannelSetupRecvBuffer, status 0x%d\n\r", status);
			return status;
		}
		status = CyU3PI2cWaitForBlockXfer (CyTrue);
		if (status != CY_U3P_SUCCESS)
		{
			return status;
		}
		status = CyU3PDmaChannelSetWrapUp ( &glI2cRxHandle );
		if (status != CY_U3P_SUCCESS)
		{
			CyU3PDebugPrint (6, "I2C read failed at CyU3PI2cWaitForBlockXfer, status 0x%x\n\r", status);
		}
		return status;
	}
	else /* Write */
	{
		/* Update the preamble information. */
		preamble.length    = 5;
		preamble.buffer[0] = devAddr<<1;
		preamble.buffer[1] = (uint8_t)(byteAddress >> 24);
		preamble.buffer[2] = (uint8_t)(byteAddress >> 16);
		preamble.buffer[3] = (uint8_t)(byteAddress >> 8);
		preamble.buffer[4] = (uint8_t)(byteAddress & 0xFF);
		preamble.ctrlMask  = 0x0000;

		buf_p.size = buffSize;
		buf_p.count = byteCount;

		status = CyU3PDmaChannelSetupSendBuffer (&glI2cTxHandle, &buf_p);
		if (status != CY_U3P_SUCCESS)
		{
			CyU3PDebugPrint (6, "I2C write failed at CyU3PDmaChannelSetupSendBuffer, status 0x%h\n\r", status);
			return status;
		}
		status = CyU3PI2cSendCommand (&preamble, byteCount, CyFalse);
		if (status != CY_U3P_SUCCESS)
		{
			CyU3PDebugPrint (6, "I2C write failed at CyU3PI2cSendCommand, status 0x%h\n\r", status);
			return status;
		}
		status = CyU3PDmaChannelWaitForCompletion(&glI2cTxHandle, CY_FX_USB_I2C_TIMEOUT);
		if (status != CY_U3P_SUCCESS)
		{
			CyU3PDebugPrint (6, "I2C write failed at CyU3PDmaChannelWaitForCompletion, status 0x%x\n\r", status);
			return status;
		}
	}

    return CY_U3P_SUCCESS;
}

void I2C_DMA_Reset(void){
	CyU3PDmaChannelReset (&glI2cTxHandle);
	CyU3PDmaChannelReset (&glI2cRxHandle);
}

/* [ ] */

