/*
 * GPIF asynchronous master FIFO initialization.
 */

#include "cyu3system.h"
#include "cyu3error.h"
#include "cyu3gpif.h"
#include "cyfxslfifosync.h"

#include "gpif_master_config.h"
#include "gpif_master.h"

void
GpifMasterInit (void)
{
    CyU3PReturnStatus_t apiRetStatus = CY_U3P_SUCCESS;

    /* Load the GPIF configuration for Slave FIFO sync mode. */
    apiRetStatus = CyU3PGpifLoad (&CyFxGpifConfig);
    if (apiRetStatus != CY_U3P_SUCCESS)
    {
        CyU3PDebugPrint (4, "CyU3PGpifLoad failed, Error Code = %d\n\r",apiRetStatus);
    }

    CyU3PGpifSocketConfigure (3,CY_U3P_PIB_SOCKET_3,6,CyFalse,1);

    /* Start the state machine. */
    apiRetStatus = CyU3PGpifSMStart (START, ALPHA_START);
    if (apiRetStatus != CY_U3P_SUCCESS)
    {
        CyU3PDebugPrint (4, "CyU3PGpifSMStart failed, Error Code = %d\n\r",apiRetStatus);
    }
}
