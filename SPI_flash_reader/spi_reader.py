"""
This script reads data from SPI flash, connected to the FX3 chip within the TSW14J56 board.
To use it please load the UsbSpiDmaMode example first into RAM of the FX3 chip using
EZUsb SDK tools.
"""
import usb.core
from usb_intf import *

filename = "dump.bin"
#Sizes are in bytes
#Micron M25P40 flash memory chip
mem_size = 524288
page_size = 256

dev = usb.core.find(idVendor = 0x04B4, idProduct = 0x00F0 )
file = open(filename, 'wb')
num_pages = int(mem_size/page_size)
page_addr = 0;

for page_addr in range(num_pages):
    print("Reading page {:d} out of {:d} pages...       ".format(page_addr, num_pages), end = '\r')
    data = dev.ctrl_transfer(0xC0, 0xC3, 0, page_addr, page_size)
    file.write(data)
print("\nDone!")
file.close()